<?php
declare(strict_types=1);
namespace Vespula\Notifier\Adapter;

/**
 * The abstract adapter class
 * 
 */
abstract class NotifierAdapter implements NotifierInterface
{
    /**
     * The message to send
     * 
     * @var string $message
     */
    protected $message;

    /**
     * The a subject that some adapters *may* use
     * 
     * @var string $subject
     */
    protected $subject;

    /**
     * Set the message string
     * 
     * @param string $message
     * @return void
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * Get the message string
     * 
     * @return string $message
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Set the subject. Not all adapter will support a subject
     * 
     * @param string $subject
     * @return void
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * Get the subject
     * 
     * @return string $subject
     */
    public function getSubject(): string
    {
        return $this->subject;
    }
}
