<?php
declare(strict_types=1);
namespace Vespula\Notifier\Adapter;

/**
 * Notification adapter interface that all adapters must implement (via the abstract adapter)
 */
interface NotifierInterface
{
    /**
     * Send the notification
     */
    public function send();

    /**
     * set the message
     */
    public function setMessage(string $message);

    /**
     * set the subject
     */
    public function setSubject(string $message);
}