<?php
declare(strict_types=1);
namespace Vespula\Notifier\Adapter;

use Laminas\Mail\Message as MailMessage;
use Laminas\Mail\Transport\TransportInterface;
use Vespula\Notifier\Exception\EmailAdapterException;

/**
 * Email adapter using Laminas Mail as the mailer. 
 */
class Email extends NotifierAdapter
{
    /**
     * The Laminas mail Transport (smtp, file, sendmail, etc)
     * 
     * @var TransportInterface
     */
    protected $mailTransport;

    /**
     * The Laminas Mail mail message
     * 
     * @var MailMessage
     */
    protected $mailMessage;

    /**
     * Constructor
     * 
     * @param MailMessage $mailMessage
     * @param TransportInterface $mailTransport
     */
    public function __construct(MailMessage $mailMessage, TransportInterface $mailTransport)
    {
        $this->mailMessage = $mailMessage;
        $this->mailTransport = $mailTransport;
    }

    /**
     * Get the mail transport
     * 
     * @return TransportInterface
     */
    public function getMailTransport(): TransportInterface
    {
        return $this->mailTransport;
    }

    /**
     * Get the Laminas Mail Message
     * 
     * @return MailMessage
     */
    public function getMailMessage(): MailMessage
    {
        return $this->mailMessage;
    }

    /**
     * Set the Laminas Mail Message
     * 
     * @return void
     */
    public function setMailMessage(MailMessage $mailMessage): void
    {
        $this->mailMessage = $mailMessage;
    }

    /**
     * Resets the mail message headers allowing slightly easier reuse of the mail message. 
     * 
     * 
     * @return void
     */
    public function reset(): void
    {
        $this->mailMessage->getHeaders()->clearHeaders();
    }

    /**
     * Reset a single header by name
     * 
     * @param string $header
     * @return bool
     */
    public function removeHeaderByName(string $header): bool
    {
        return $this->mailMessage->getHeaders()->removeHeader($header);
    }

    /**
     * Send the notification by email
     * 
     * @return void
     * @throws EmailAdapterException
     */
    public function send(): void
    {
        $this->mailMessage->setBody($this->message);

        if ($this->subject) {
            $this->mailMessage->setSubject($this->subject);
        }

        try {
            $this->mailTransport->send($this->mailMessage);
        } catch (EmailAdapterException $e) {
            throw $e;
        }
            
    }
}