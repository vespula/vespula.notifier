<?php
declare(strict_types=1);

namespace Vespula\Notifier\Adapter;
use Vespula\Notifier\Adapter\NotifierInterface;

/**
 * An AdapterList class (ArrayObject) which holds the adapters. 
 * 
 * This ensures that adapters are added correctly with a string key and a proper adapter interface
 */

class AdapterList extends \ArrayObject
{

    /**
     * Append a new item in the store
     * 
     * @param string $key Identifier for the adapter
     * @param NotifierInterface $adapter The adapter
     * @throws InvalidArgumentException
     */
    public function offsetSet($key, $adapter): void
    {
        if (! is_string($key)) {
            throw new \InvalidArgumentException();
        }
        
        if (! $adapter instanceof NotifierInterface) {
            throw new \InvalidArgumentException('value must be an instance of ' . NotifierInterface::class);
        }
        
        parent::offsetSet($key, $adapter);
    }
    
}