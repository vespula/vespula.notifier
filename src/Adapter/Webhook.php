<?php
declare(strict_types=1);
namespace Vespula\Notifier\Adapter;

use Vespula\Notifier\Exception\InvalidUrlException;
use Vespula\Notifier\Exception\CurlException;

/**
 * Send a notification to a webhook, such as Slack, Mattermost, or MS Teams
 */
class Webhook extends NotifierAdapter
{
    /**
     * The Webhook URL
     * 
     * @var string
     */
    protected $url;

    /**
     * The json key for the text message. Default is 'text'
     * 
     * @var string
     */
    protected $text_param = 'text';

    /**
     * Additional, optional params for the payload. See individual services for possible values. Eg, Discord has username, avatar_url, etc.
     * 
     * @var array
     */
    protected $params = [];


    /**
     * Constructor
     * 
     * @param string $url The webhook url
     */
    public function __construct(string $url)
    {
        if ($url) {
            $this->setUrl($url);
        }
        
    }

    /**
     * Set the url. Ensures the url is valid
     * 
     * @param string $url
     * @return void
     * @throws InvalidUrlException
     */
    public function setUrl(string $url): void
    {
        $valid_url = filter_var($url, FILTER_VALIDATE_URL);
        if (! $valid_url) {
            throw new InvalidUrlException('Invalid URL: ' . $url);
        }

        $this->url = $url;
    }

    /**
     * Set the text parameter for the json payload. Default is text
     * 
     * @param string $param
     * @return void
     */
    public function setTextParam(string $param): void
    {
        $this->text_param = $param;
    }

    /**
     * Get the text param
     * 
     * @return string
     */
    public function getTextParam(): string
    {
        return $this->text_param;
    }

    /**
     * Set a single param by key
     * 
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function setParam(string $key, $value): void
    {
        $this->params[$key] = $value;
    }

    /**
     * Set an array of params all at once by keys and values
     * 
     * @param array $params
     * @return voic
     */
    public function setParams(array $params): void
    {
        $this->params = array_merge($this->params, $params);
    }

    /**
     * Get the param array
     * 
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * Encode the params including the text param
     * 
     * @return string
     */
    protected function encodeParams(): string
    {
        $content = $this->message;
        if ($this->subject) {
            $content = "**" . $this->subject . "**\n" . $content;
        }
        $this->params[$this->text_param] = $content;

        $payload = json_encode($this->params);

        return $payload;
    }

    /**
     * Send the message to the webhook url using CURL
     * 
     * @return void
     */
    public function send(): void
    {
        // Use curl to send

        $payload = $this->encodeParams();


        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
 
        // Submit the POST request
        $result = curl_exec($ch);

        if ($result === false) {
            throw new CurlException('Curl error: ' . curl_error($ch));
        }
        
        // Close cURL session handle
        curl_close($ch);
    }
}