<?php
declare(strict_types=1);
namespace Vespula\Notifier\Adapter;

/**
 * Send a message to PHP's stdout (generally for testing);
 */
class Stdout extends NotifierAdapter
{
    /**
     * Sends the message to stdout
     * 
     * @return void
     */
    public function send(): void
    {
        $stdout = fopen('php://stdout', 'w');

        if ($this->subject) {
            $this->message = "[" . $this->subject . "] " . $this->message;
        }

        fwrite($stdout, $this->message . PHP_EOL);
        fclose($stdout);
    }
}