<?php
declare(strict_types=1);
namespace Vespula\Notifier\Adapter;

use Vespula\Notifier\Exception\InvalidUrlException;
use Vespula\Notifier\Exception\CurlException;

/**
 * Send a notification to a webhook, such as Slack, Mattermost, or MS Teams
 */
class Discord extends Webhook
{
    /**
     * The json key for the text message. Discord uses 'contnet'
     * 
     * @var string
     */
    protected $text_param = 'content';
}