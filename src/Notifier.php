<?php 
declare(strict_types=1);
namespace Vespula\Notifier;

use ArrayObject;
use Vespula\Notifier\Adapter\NotifierInterface;
use Vespula\Notifier\Exception\InvalidAdapterException;
use Vespula\Notifier\Adapter\AdapterList;

class Notifier
{
    /**
     * An ArrayObject that contains the adapters keyed by a string identifier
     * 
     * @var AdapterList 
     */
    protected $adapters;

    /**
     * Constructor
     * 
     * @return void
     */
    public function __construct()
    {
        $this->adapters = new AdapterList();
        $this->adapters->setFlags(\ArrayObject::ARRAY_AS_PROPS);
    }

    /**
     * Add an adapter to the $adapters AdapterList
     * 
     * @param string $key The identifier for the adapter
     * @param NotifierInterface $adapter The adapter
     * 
     * @return void
     */
    public function addAdapter(string $key, NotifierInterface $adapter): void
    {
        $this->adapters[$key] = $adapter;
    }

    /**
     * Set all adapters at once by passing an AdapterList of adapters
     * 
     * @param AdapterList $adapters
     * @return void
     */
    public function setAdapters(AdapterList $adapters): void
    {
        $this->adapters = $adapters;
    }

    /**
     * Get an adapter by key
     * 
     * @param string $key The adapter identifier
     * @throws InvalidAdapterException
     */
    public function getAdapter(string $key): NotifierInterface
    {
        $adapter = $this->adapters[$key] ?? null;
        if (is_null($adapter)) {
            throw new InvalidAdapterException('Adapter does not exist');
        }

        return $adapter;
    }

    /**
     * Get all current adapters as an AdapterList
     * 
     * @return AdapterList 
     */
    public function getAdapters(): AdapterList
    {
        return $this->adapters;
    }

    /**
     * Set the message on all or specific adapters
     * 
     * You can specify specific adapters by passing an array of adapter keys as the second
     * argument
     * 
     * @param string $message
     * @param array $keys Adapter keys (null by default)
     * @return void
     */
    public function setMessage(string $message, array $keys = null): void
    {
        $adapters = $this->getAdapters();
        if ($keys) {
            $adapters = [];
            foreach ($keys as $key) {
                $adapters[] = $this->getAdapter($key);
            }
        }

        foreach ($adapters as $adapter) {
            $adapter->setMessage($message);
        }
    }

    /**
     * Set the subject on all or specific adapters. Not all adapters will support or use subjects
     * 
     * You can specify specific adapters by passing an array of adapter keys as the second
     * argument
     * 
     * @param string $subject
     * @param array $keys Adapter keys (null by default)
     * @return void
     */
    public function setSubject(string $subject, array $keys = null): void
    {
        $adapters = $this->getAdapters();
        if ($keys) {
            $adapters = [];
            foreach ($keys as $key) {
                $adapters[] = $this->getAdapter($key);
            }
        }

        foreach ($adapters as $adapter) {
            $adapter->setSubject($subject);
        }
    }
    
    /**
     * Trigger the notification on all the adapters, or specific adapters
     * 
     * Pass an array of adapter keys to specify which adapters should trigger
     * 
     * @param array $keys The adapter keys
     * @return void
     */
    public function notify(array $keys = null): void
    {
        if ($keys) {
            foreach ($keys as $key) {
                $adapter = $this->getAdapter($key);
                $adapter->send();
            }

            return;
        }
        foreach ($this->adapters as $adapter) {
            $adapter->send();
        }
    }
}