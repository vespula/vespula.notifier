<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use Vespula\Notifier\Adapter\AdapterList;
use Vespula\Notifier\Adapter\Stdout;



final class AdapterListTest extends TestCase
{
    protected $list;
    

    protected function setUp(): void
    {
       
        $this->list = new AdapterList();
        
    }
    public function testOffsetSet()
    {
        $stdout = new Stdout();
        $this->list['stdout'] = $stdout;

        $this->assertEquals($stdout, $this->list['stdout']);
    }

    public function testOffsetSetException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $stdout = new Stdout();
        $this->list[0] = $stdout;
    }

    public function testOffsetSetExceptionAdapter()
    {
        $this->expectException(\InvalidArgumentException::class);
        
        $this->list['foo'] = 'a string';
    }
    
}
