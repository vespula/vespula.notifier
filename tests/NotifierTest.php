<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use Vespula\Notifier\Notifier;
use Vespula\Notifier\Adapter\Stdout;
use Vespula\Notifier\Adapter\Email;
use Vespula\Notifier\Adapter\Webhook;
use Laminas\Mail\Message as MailMessage;
use Laminas\Mail\Transport\InMemory as MailTransport;
use Vespula\Notifier\Exception\InvalidAdapterException;


final class NotifierTest extends TestCase
{
    protected $notifier;
    protected $stdoutAdapter;
    protected $mailAdapter;
    protected $webhookAdapter;

    protected function setUp(): void
    {
        $this->stdoutAdapter = new Stdout();
        $this->webhookAdapter = new Webhook('http://0.0.0.0');
        $transport = new MailTransport();
        $message = new MailMessage();
        $this->mailAdapter = new Email($message, $transport);
        $this->notifier = new Notifier();
        
    }
    public function testAddAdapter()
    {
        $this->notifier->addAdapter('stdout', $this->stdoutAdapter);
        $expected = $this->notifier->getAdapter('stdout');

        $this->assertEquals($expected, $this->stdoutAdapter);
    }

    public function testGetAdapterException()
    {
        $this->expectException(InvalidAdapterException::class);
        $this->notifier->getAdapter('none');
        
    }

    public function testSetAdapters()
    {
        $adapters = $this->notifier->getAdapters();
        $adapters['stdout'] = $this->stdoutAdapter;
        $adapters['email'] = $this->mailAdapter;
        $this->notifier->setAdapters($adapters);
        $expected = $this->notifier->getAdapters();

        $this->assertEquals($adapters, $expected);
    }

    public function testSetMessage()
    {
        $expected = 'the message';
        $adapters = $this->notifier->getAdapters();
        $adapters['stdout'] = $this->stdoutAdapter;
        $adapters['email'] = $this->mailAdapter;
        $this->notifier->setAdapters($adapters);
        $this->notifier->setMessage($expected);
        
        foreach ($this->notifier->getAdapters() as $adapter) {
            $this->assertEquals($expected, $adapter->getMessage());
        }

        $this->notifier->setMessage('a test', ['stdout']);
        $this->assertEquals('a test', $this->stdoutAdapter->getMessage()); 
    }

    public function testSetSubject()
    {
        $expected = 'the subject';
        $adapters = $this->notifier->getAdapters();
        $adapters['stdout'] = $this->stdoutAdapter;
        $adapters['email'] = $this->mailAdapter;
        $this->notifier->setAdapters($adapters);
        $this->notifier->setSubject($expected);
        
        foreach ($this->notifier->getAdapters() as $adapter) {
            $this->assertEquals($expected, $adapter->getSubject());
        }

        $this->notifier->setSubject('a test', ['stdout']);
        $this->assertEquals('a test', $this->stdoutAdapter->getSubject()); 
    }
    
}
