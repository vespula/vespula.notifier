<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use Vespula\Notifier\Adapter\Stdout;

final class StdoutTest extends TestCase
{
    protected $adapter;

    protected function setUp(): void
    {
        $this->adapter = new Stdout();
    }
    public function testSetMessage()
    {
        $expected = 'mymessage';
        $this->adapter->setMessage('mymessage');
        $this->assertEquals($expected, $this->adapter->getMessage());
    }
    
}
