<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use Vespula\Notifier\Adapter\Webhook;
use Vespula\Notifier\Exception\InvalidUrlException;

final class WebhookTest extends TestCase
{
    protected $adapter;
    protected $mailMessage;
    protected $mailTransport;

    protected function setUp(): void
    {
        $url = 'http://localhost:8000';
        $this->adapter = new Webhook($url);
    }
    public function testGetSetTextParam()
    {
        $expected = 'param';
        
        $this->adapter->setTextParam('param');
        $actual = $this->adapter->getTextParam();
        $this->assertEquals($expected, $actual);
    }

    public function testGetSetParam()
    {
        $expected = ['param'=>'value'];
        
        $this->adapter->setParam('param', 'value');
        $actual = $this->adapter->getParams();
        $this->assertEquals($expected, $actual);
    }

    public function testGetSetParams()
    {
        $expected = ['param'=>'value', 'other'=>'different'];
        
        $this->adapter->setParams([
            'param'=>'value',
            'other'=>'different'
        ]);
        $actual = $this->adapter->getParams();
        $this->assertEquals($expected, $actual);
    }

    public function testSetUrl()
    {
        $this->expectException(InvalidUrlException::class);
        $url = 'bogus url';
        $this->adapter = new Webhook($url);
    }

    
}
