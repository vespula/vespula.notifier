<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use Vespula\Notifier\Adapter\Email;
use Laminas\Mail\Message as MailMessage;
use Laminas\Mail\Transport\InMemory as MailTransport;

final class EmailTest extends TestCase
{
    protected $adapter;
    protected $mailMessage;
    protected $mailTransport;

    protected function setUp(): void
    {
        $mailMessage = new MailMessage();
        $mailTransport = new MailTransport();

        $mailMessage->setTo('bill@example.com');
        $mailMessage->setSubject('Test');
        $this->mailMessage = $mailMessage;
        $this->mailTransport = $mailTransport;
        
        $this->adapter = new Email($mailMessage, $mailTransport);
    }
    public function testSend()
    {
        $this->mailMessage->setBody('mymessage');
        $this->mailTransport->send($this->mailMessage);
        $expected = $this->mailTransport->getLastMessage();
        $this->adapter->setMessage('mymessage');
        $this->adapter->send();
        $actual = $this->mailTransport->getLastMessage();
        $this->assertEquals($expected, $actual);
    }

    public function testSetMailMessage()
    {
        
        $messageTwo = new MailMessage();
        $messageTwo->setCc('foo@bar.com');

        $this->adapter->setMailMessage($messageTwo);
        $this->assertNotEquals($this->adapter->getMailMessage(), $this->mailMessage);
        $this->assertNotEquals($messageTwo->getCc(), $this->mailMessage->getCc());
    }

    public function testReset()
    {
        $this->assertEquals('bill@example.com', $this->mailMessage->getTo()->current()->getEmail());
        $this->adapter->reset();

        $this->assertEquals(0, count($this->mailMessage->getTo()));

        
    }

    public function testRemoveHeaderByName()
    {
        $this->mailMessage->setCc('foo@example.com');
        $this->assertEquals('foo@example.com', $this->mailMessage->getCc()->current()->getEmail());
        $this->adapter->removeHeaderByName('cc');

        $this->assertEquals(0, count($this->mailMessage->getCc()));

        
    }

    
}
